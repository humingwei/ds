//
// Created by humingwei on 2024/10/2.
//排序算法

#ifndef DS_SORT_H
#define DS_SORT_H

#endif //DS_SORT_H
#include <iostream>
using namespace std;//sway函数用得到
#include "tool.h"
//#include "struct.h"

//RecType R[]={9,8,7,6,5,4,3,2,1,0};//插入排序数组
//RecType R[]={6,8,7,9,0,1,3,2,4,5};//归并排序数组

void InsertSort(RecType R[],int n){//直接插入排序
    int i,j;
    RecType tmp;
    for ( i = 1; i < n; i++) {
        if(R[i].key<R[i-1].key){
            tmp.key=R[i].key;
            j=i-1;
            do {
                R[j+1]=R[j];
                j--;
            } while (j>=0&&R[j].key>tmp.key);
            R[j+1]=tmp;
        }
    }
    Print(R,n);

}
void BinInsertSOrt(RecType R[],int n){//折半插入排序
    int i,j,low,high,mid;
    RecType tmp;
    for ( i = 1; i < n; i++) {
        if (R[i].key<R[i-1].key){
            tmp=R[i];
            low=0;
            high=i-1;
            while (low<high){
                mid=(low+high)/2;
                if (tmp.key<R[mid].key){
                    high=mid-1;
                } else{
                    low=mid+1;
                }
                for ( j = i-1; j >=high+1 ; j--) {
                    R[j+1]=R[j];
                }
                R[high]=tmp;
            }

        }
    }
    Print(R,n);
}
void ShellSort(RecType R[],int n) {//希尔排序
    int i, j, d;
    RecType tmp;
    d = n / 2;
    while (d > 0) {
        for (i = d; i < n; i++) {
            tmp = R[i];
            j = i - d;
            while (tmp.key < R[j].key) {
                R[j + d] = R[j];
                j = j - d;
            }
            R[j + d] = tmp;
        }
        d = d / 2;
    }
}
void BubbleSort(RecType R[],int n){//冒泡排序
    int i,j;
    bool exchange;
    for (i = 0; i < n-1; i++) {
        exchange= false;
        for (j=n-1 ; j >i ; j--) {
            if(R[j].key<R[j-1].key){
                swap(R[j],R[j-1]);
                exchange= true;
            }
        }
        if (!exchange){
            return;
        }
    }
}
int partition(RecType R[],int s,int t){//快排分割
    int i=s,j=t;
    RecType tmp=R[s];
    while (i<j){
        while (i<j&&R[j].key>tmp.key){
            j--;
        }
        R[i]=R[j];
        while (i<j&&R[i].key<tmp.key){
            i++;
        }
        R[j]=R[i];
    }
    R[i]=tmp;
    return  i;
}
void QuickSort(RecType R[],int s,int t){//快排主体
    if(s<t){
        int i=partition(R,s,t);
        QuickSort(R,s,i-1);
        QuickSort(R,i+1,t);
    }}

void SelectSort(RecType R[],int n){//简单选择排序
    int i,j,k;
    for (i = 0; i <n ; i++) {
        k=i;//让最小值指针先等于i
        for (j = i+1; j <n ; j++) {
            if (R[k].key>R[j].key){
                k=j;
            }
        }
        swap(R[i],R[k]);
    }
}
void sift(RecType R[],int low,int high){//仅对一个父节点之下的树筛选    堆排序-筛选
    int i=low,j=i*2;
    RecType tmp=R[i];
    while (j<high){
        if (R[j].key<R[j+1].key){//找最大的孩子
            j++;
        }
        if (tmp.key<R[j].key){//让最大的孩子和父节点比较大小
            R[i]=R[j];//此时原父节点消失，所有需要tmp
            i=j;
            j=i*2;
        } else
        {
            break;
        }
        R[i]=tmp;//原父结点已转移到叶子结点
    }

}
void HeapSort(RecType R[],int n){//堆排序-主体
    for (int i = n/2; i >=1 ; i--) {//初始堆
        sift(R,i,n);
    }
    for (int i = n; i >1 ; i--) {
        swap(R[1],R[i]);
        sift(R,1,i-1);//初始堆整体有序后，可以只筛选根节点
    }
}
void MergeSort(RecType R[],int low,int mid,int high){//归并排序-归并  要求两个有序表
    int i=low,j=mid,k=0;
    RecType *R1= (RecType *)malloc((high-low+1)*sizeof(RecType));
    InitializeArray(R1,10);
    while (i<=mid&&j<=high){
        if (R[i].key<=R[j].key){
            R1[k]=R[i];
            i++;
            k++;
        } else{
            R1[k]=R[j];
            j++;
            k++;
        }
    }
    while (i<=mid){
        R1[k]=R[i];
        k++;
        i++;
    }
    while (j<=high){
        R1[k]=R[j];
        k++;
        j++;
    }
    for (i=0, k = 0; k <=high ; k++,i++) {
        R[i]=R1[k];
    }
    free(R1);
//    R1=NULL;
}